package com.org.expense.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.org.expense.models.Expense;



@Repository
public interface ExpenseRepository extends CrudRepository<Expense, Integer>{
	
	
	

	@Query("select e from Expense e where e.purchaseDate >=?1 and e.purchaseDate<=?2")
	Iterable<Expense> getExpensesByMonth(Date fromDate ,Date toDate);



}
