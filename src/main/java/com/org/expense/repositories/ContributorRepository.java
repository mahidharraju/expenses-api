package com.org.expense.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.org.expense.models.Contributor;

@Repository
public interface ContributorRepository extends CrudRepository<Contributor, Integer>{
	
	

}
