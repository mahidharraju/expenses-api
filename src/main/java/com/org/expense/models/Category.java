package com.org.expense.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@JsonFormat
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Category {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CATEGORY_ID")
	private Integer id;
	
	@Column(name = "NAME")
	private String name;
	

	@OneToMany(
	        mappedBy = "expenseCategory",
	        cascade = CascadeType.PERSIST,
	        fetch = FetchType.LAZY
	    )
	@JsonIgnore
	private Set<Expense> expenses;
	
	
	public Category(Integer id, String name) {
		
		this.id = id;
		this.name = name;
		expenses = new HashSet<Expense>();
	}
	public Category() {
		expenses = new HashSet<Expense>();
	}
	
	public Category(Integer id) {
		this.id =id;
		expenses = new HashSet<Expense>();
	}
	
	
		
	public Category(Integer id, String name, Set<Expense> expenses) {
		super();
		this.id = id;
		this.name = name;
		this.expenses = expenses;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public Set<Expense> getExpenses() {
		return expenses;
	}
	public void setExpenses(Set<Expense> expenses) {
		this.expenses = expenses;
		 for (Expense expense : expenses) {
	            expense.setCategory(this);
	        }
	}
	
	
	
	
	
	

}
