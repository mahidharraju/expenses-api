package com.org.expense.models;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Expense {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "EXPENSE_ID")
	private Integer id;
	
	private String expName;
	
	@ManyToOne(fetch = FetchType.LAZY , targetEntity = Category.class)
	@JoinColumn(name = "CATEGORY_ID" )
	private Category expenseCategory;

	
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contributor.class)
	@JoinColumn(name = "CONTRIBUTOR_ID")
	private Contributor expenseContributor;
	
	private double amount;
    private Date  purchaseDate;
    private String comments;
    
    
    
    
	public Expense(Integer id, String expName, Category category, Contributor contributor, double amount, Date purchaseDate,
			String comments) {
		super();
		this.id = id;
		this.expName = expName;
		this.expenseCategory = category;
		this.expenseContributor = contributor;
		this.amount = amount;
		this.purchaseDate = purchaseDate;
		this.comments = comments;
	}
	
	
	
	
	public Expense() {
	}
	public String getExpName() {
		return expName;
	}
	public void setExpName(String expName) {
		this.expName = expName;
	}
	public Category getCategory() {
		return expenseCategory;
	}
	public void setCategory(Category category) {
		this.expenseCategory = category;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return purchaseDate;
	}
	public void setDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Contributor getContributor() {
		return expenseContributor;
	}

	public void setContributor(Contributor contributor) {
		this.expenseContributor = contributor;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	} 
	
	
 
    
    //TODO USE builder to avoid construtor
    
    

}
