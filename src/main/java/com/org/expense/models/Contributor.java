package com.org.expense.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Contributor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CONTRIBUTOR_ID")
	private Integer id;
	
	private String contributorName;
	
	private String contributorType;
	
	@OneToMany(
	        mappedBy = "expenseContributor",
	        cascade = CascadeType.PERSIST,
	        fetch = FetchType.LAZY
	    )
	@JsonIgnore
	private Set<Expense> expenses;
	
	public Contributor() {
		
		expenses = new HashSet<Expense>();
		
	}
	
	public Contributor(int id, String contributorName, String contributorType) {
		this.id = id;
		this.contributorName = contributorName;
		this.contributorType = contributorType;
		
		expenses = new HashSet<Expense>();
	}
	
	
	
	public Contributor(Integer id, String contributorName, String contributorType, Set<Expense> expenses) {
		super();
		this.id = id;
		this.contributorName = contributorName;
		this.contributorType = contributorType;
		this.expenses = expenses;
		expenses = new HashSet<Expense>();
	}

	public Contributor(Integer id) {
		this.id =id;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContributorName() {
		return contributorName;
	}

	public void setContributorName(String contributorName) {
		this.contributorName = contributorName;
	}

	public String getContributorType() {
		return contributorType;
	}

	public void setContributorType(String contributorType) {
		this.contributorType = contributorType;
	}
	
	
	public Set<Expense> getExpenses() {
		return expenses;
	}
	public void setExpenses(Set<Expense> expenses) {
		this.expenses = expenses;
		 for (Expense expense : expenses) {
	            expense.setContributor(this);
	        }
	}
	

}
