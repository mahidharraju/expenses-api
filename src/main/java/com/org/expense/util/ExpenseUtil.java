package com.org.expense.util;

import java.util.Calendar;
import java.sql.Date;
import java.time.YearMonth;

public class ExpenseUtil {

	public static MonthRange calculateMonthRange(int month, int year) {
		// Tune
		
		Date statDay = new Date(year-1900, month-1, 1);
		YearMonth yearMonthObject = YearMonth.of(year, month);
		int daysInMonth = yearMonthObject.lengthOfMonth();
		Date endDay = new Date(year-1900, month-1, daysInMonth);
		System.out.println(statDay+"::"+endDay);
		
		return new MonthRange(statDay, endDay);
	}

}
