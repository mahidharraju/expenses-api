package com.org.expense.util;

import java.sql.Date;

public class MonthRange {
	
	private Date startDayOfMonth;
	private Date endDayOfMonth;
	
	
	public MonthRange(Date startDayOfMonth, Date endDayOfMonth) {
		super();
		this.startDayOfMonth = startDayOfMonth;
		this.endDayOfMonth = endDayOfMonth;
	}


	public Date getStartDayOfMonth() {
		return startDayOfMonth;
	}


	public void setStartDayOfMonth(Date startDayOfMonth) {
		this.startDayOfMonth = startDayOfMonth;
	}


	public Date getEndDayOfMonth() {
		return endDayOfMonth;
	}


	public void setEndDayOfMonth(Date endDayOfMonth) {
		this.endDayOfMonth = endDayOfMonth;
	}
	
	
	
	
	
	

}
