package com.org.expense.controllers;

import java.sql.Date;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.expense.models.Expense;
import com.org.expense.services.ExpenseService;

@RestController
@CrossOrigin
public class ExpenseController {
	
	@Autowired
	ExpenseService expenseService;
	
	@PostMapping(path="/createExpense")
	public ResponseEntity<Expense> createExpense(@RequestBody Expense exp)
	{
		System.out.println("exp::"+exp.getAmount());
		Expense newExp;
		if(exp!=null)
		{
			if(exp.getId()!=null)
			{	
				newExp=expenseService.updateExpense(exp);
			}
			newExp=expenseService.createExpense(exp);
			return new ResponseEntity(newExp,HttpStatus.OK);
		}
		return new ResponseEntity(null,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@GetMapping("/getAllExpenses")
	public ResponseEntity<List<Expense>> getAllExpenses()
	{
		List<Expense> expensesList=expenseService.getAllExpenses();
		return new ResponseEntity(expensesList,HttpStatus.OK);

	}
	
	
	@GetMapping("/getAllExpenses/{month}/{year}")
	public ResponseEntity<List<Expense>> getAllExpenses(@PathVariable("month") String fromDate, @PathVariable("year") String toDate)
	{
		System.out.println(fromDate+","+toDate);
		try {
			List<Expense> expensesList=expenseService.getExpensesByDateRange(fromDate,toDate);
			return new ResponseEntity(expensesList,HttpStatus.OK);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	@DeleteMapping("/deleteExpense/{expenseId}")
	public void deleteExpense(@PathVariable("expenseId") int expenseId )
	{
		expenseService.deleteExpense(expenseId);
	}
	{
		
	}

}
