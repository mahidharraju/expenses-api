package com.org.expense.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.expense.models.Contributor;
import com.org.expense.services.ContributorService;

@RestController
@CrossOrigin
public class ContributorController {
	
	
	@Autowired
	ContributorService contributorService;
	
	@GetMapping("/getAllContributors")
	public ResponseEntity<List<Contributor>> getAllContributors()
	{
		return new ResponseEntity(contributorService.getAllContributors(),HttpStatus.OK);
	}

}
