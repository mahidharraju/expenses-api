package com.org.expense.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.expense.models.Category;
import com.org.expense.services.CategoryService;

@RestController
@CrossOrigin
public class CategoriesController {
	
	@Autowired
	CategoryService categoryService;
	
	
	@GetMapping(path="/getAllCatogeries",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Category>> getAllCatogeries()
	{
		return new ResponseEntity(categoryService.getAllCatogeries(),HttpStatus.OK);
		//return new ResponseEntity((categoryService.getAllCatogeries()).stream().map(Category::getName).collect(Collectors.toList()),HttpStatus.OK);
		
	}
	
	@PostMapping(path="/createCategory")
	public ResponseEntity createCategory(@RequestBody String name)
	{
		System.out.println("Test");
		categoryService.createCategory(name);
		return new ResponseEntity(HttpStatus.OK);
		
	}

}
