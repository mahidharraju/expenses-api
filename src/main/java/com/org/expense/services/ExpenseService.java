package com.org.expense.services;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.org.expense.models.Category;
import com.org.expense.models.Expense;
import com.org.expense.repositories.CategoryRepository;
import com.org.expense.repositories.ExpenseRepository;
import com.org.expense.util.ExpenseUtil;
import com.org.expense.util.MonthRange;

@Service
@Transactional
public class ExpenseService {

	
	@Autowired
	ExpenseRepository expenseRepository;
	
	@Autowired
	CategoryRepository categRepository;
	
	
	@Autowired
	EntityManager entityManager;
	
	public Expense createExpense(Expense exp) {
		
		return expenseRepository.save(exp);
	}

	public List<Expense> getExpensesByDateRange(String from, String to) throws ParseException {
		//MonthRange monthRange=ExpenseUtil.calculateMonthRange(month, year);
		Date fromDate=new SimpleDateFormat("yyyy-MM-dd").parse(from);
		Date toDate=new SimpleDateFormat("yyyy-MM-dd").parse(to);

		Iterable<Expense> allExpenses= expenseRepository.getExpensesByMonth(fromDate, toDate);
		 List<Expense> expenseList= new ArrayList();
			
		 allExpenses.forEach(expenseList::add);
		 
		 return expenseList;
	}

	public void deleteExpense(int expenseId) {
		expenseRepository.deleteById(expenseId);
		
	}

	public List<Expense> getAllExpenses() {
		 List<Expense> expenseList= new ArrayList();

		Iterable<Expense> allExpenses= expenseRepository.findAll();
		allExpenses.forEach(expenseList::add);
		 
		return expenseList;
	}

	public Expense updateExpense(Expense exp) {
		return expenseRepository.save(exp);
		//return null;
	}

}
