package com.org.expense.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.expense.models.Category;
import com.org.expense.models.Contributor;
import com.org.expense.repositories.ContributorRepository;

@Service
public class ContributorService {

	@Autowired
	ContributorRepository contributorRepository;
	
	public List<Contributor> getAllContributors() {
		List<Contributor> contributors = new ArrayList<Contributor>();
		Iterable<Contributor> allContributors = contributorRepository.findAll();
		allContributors.forEach(contributors::add);
		return contributors;
		
	}
	
	

}
