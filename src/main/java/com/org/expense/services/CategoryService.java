package com.org.expense.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.org.expense.models.Category;
import com.org.expense.repositories.CategoryRepository;

@Service
@Transactional
public class CategoryService {
	
	@Autowired
	CategoryRepository categoryRepo;
	
    private static final Logger logger = LogManager.getLogger(CategoryService.class);

	
	public List<Category> getAllCatogeries() {
		
		logger.debug("Calling Get All Categories");
		Iterable<Category> allCategories = categoryRepo.findAll();
		List<Category> categoriesList= new ArrayList();
		
		allCategories.forEach(categoriesList::add); 
		
		return categoriesList;
		
	}
	
	
	public void createCategory(String categoryName)
	{
		System.out.println("Entered");
		Category currentCategory = new Category(null,categoryName);
		categoryRepo.save(currentCategory);
	}

}
